package com.tatvic.lib.tvcdeeplinking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ShopActivity extends AppCompatActivity {
    TextView shop_tb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        shop_tb=(TextView)findViewById(R.id.shop_tb);
        String text= (String) shop_tb.getText();
        Intent intentfrom=getIntent();
        String intentData= String.valueOf(intentfrom.getData());
        shop_tb.setText(text+"\n Came From: "+intentData);
    }
}
