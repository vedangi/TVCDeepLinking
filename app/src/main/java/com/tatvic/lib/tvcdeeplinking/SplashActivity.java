package com.tatvic.lib.tvcdeeplinking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {
    TextView splash_tb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splash_tb=(TextView)findViewById(R.id.splash_tb);
        String text= (String) splash_tb.getText();
        Intent intentfrom=getIntent();
        String intentData= String.valueOf(intentfrom.getData());
        splash_tb.setText(text+"\n Came From: "+intentData);
    }


}
